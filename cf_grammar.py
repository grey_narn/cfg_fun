#!/usr/bin/env python


def get_all_subsets(a):
    """return list of all subsets of a"""
    result = [[]]
    for x in a:
        result.extend([subset + [x] for subset in result])
    return result


def get_all_nonempty_subsets(a):
    """return list of all subsets of a"""
    result = [[]]
    for x in a:
        result.extend([subset + [x] for subset in result])
    result.remove([])
    return result


class Symbol:

    next_idx = 0
    symbol_list = []

    def __init__(self, is_terminal, text):
        self.idx = Symbol.next_idx
        Symbol.next_idx += 1
        Symbol.symbol_list.append(self)
        self._is_terminal = is_terminal
        self.text = text

    def is_terminal(self):
        return self._is_terminal

    def __repr__(self):
        return "id = {}, is_terminal = {}, text = {}".format(self.idx,
                                                             self.is_terminal(),
                                                             self.text)

    def __str__(self):
        return self.text


def get_new_symbol(text):
    return Symbol(False, text)


def get_new_terminal(text):
    return Symbol(True, text)


epsilon = get_new_terminal("epsilon")


def get_symbol_by_index(idx):
    return Symbol.symbol_list[idx]


class Rule:

    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = [x for x in rhs if x != epsilon]
        if self.rhs == []:
            self.rhs = [epsilon]

    def __repr__(self):
        return "{} -> {}".format(repr(self.lhs), repr(self.rhs))

    def __str__(self):
        rhs_string = " ".join([str(s) for s in self.rhs])
        return "{} -> {}".format(str(self.lhs), rhs_string)

    def is_unit(self):
        return len(self.rhs) == 1 and not self.rhs[0].is_terminal()

    def is_epsilon_rule(self):
        return self.rhs == [epsilon]


class Grammar:
    """Represent a CFG."""

    def __init__(self, rules):
        self.rules = rules
        pass

    def add_rule(self, lhs, rhs):
        self.rules.append(Rule(lhs, rhs))

    def remove_rule(self, rule):
        self.rules.remove(rule)

    def get_all_epsilon_rules(self):
        return filter(lambda x: x.is_epsilon_rule(), self.rules)

    def get_all_non_epsilon_rules(self):
        return filter(lambda x: not x.is_epsilon_rule(), self.rules)

    def set_start_symbol(self, new_start_symbol):
        self.start_symbol = new_start_symbol

    def add_new_start_symbol(self):
        new_start_symbol = get_new_symbol(str(self.start_symbol) + "_'")
        self.add_rule(new_start_symbol, [self.start_symbol])
        self.set_start_symbol(new_start_symbol)

    def delete_unit_rules(self):
        not_done = True
        while not_done:
            not_done = False
            for ur in [r for r in self.rules if r.is_unit()]:
                # print("Deleting unit rule {}".format(ur))
                not_done = True
                for rule in [x for x in self.rules if not x == ur]:
                    if rule.lhs == ur.rhs[0]:
                        self.add_rule(ur.lhs, rule.rhs)
                self.remove_rule(ur)

    def delete_terminals_from_rhs(self):
        nonterminal_to_new_symb_dict = {}
        for rule in [r for r in self.rules if len(r.rhs) > 1]:
            new_rhs = []
            for symb in rule.rhs:
                if not symb.is_terminal():
                    new_rhs.append(symb)
                else:
                    new_lhs = nonterminal_to_new_symb_dict.get(symb)
                    if new_lhs is None:
                        new_lhs = get_new_symbol("N_FOR_" + str(symb) + " ")
                        nonterminal_to_new_symb_dict[symb] = new_lhs
                        self.add_rule(new_lhs, [symb])
                    new_rhs.append(new_lhs)
            if new_rhs != rule.rhs:
                self.add_rule(rule.lhs, new_rhs)
                self.remove_rule(rule)

    def get_nullable_symbols(self):
        result = set()
        for rule in self.get_all_epsilon_rules():
            result.add(rule.lhs)
        found_new = True
        other_rules = self.get_all_non_epsilon_rules()
        while found_new:
            found_new = False
            for rule in other_rules[:]:
                for rhs_symb in rule.rhs:
                    if rhs_symb.is_terminal():
                        # this rule cannot produce epsilon
                        other_rules.remove(rule)
                        break
                    if rhs_symb not in result:
                        break
                else:
                    # no break -> this rule can produce epsilon
                    result.add(rule.lhs)
                    other_rules.remove(rule)
                    found_new = True
        return result

    def delete_epsilon_rules(self):
        nullable = self.get_nullable_symbols()
        for rule in self.get_all_non_epsilon_rules():
            nullable_occurences = [i for i, x in enumerate(rule.rhs)
                                   if x in nullable]
            for occur_ss in get_all_nonempty_subsets(nullable_occurences):
                new_rhs = [x for i, x in enumerate(rule.rhs) if i not in occur_ss]
                if len(new_rhs) > 0:
                    self.add_rule(rule.lhs, new_rhs)
        for eps_rule in self.get_all_epsilon_rules()[:]:
            self.remove_rule(eps_rule)

    def binarize(self):
        # replace A -> X_1 X_2 ... X_n with
        # A -> X_1 A_1, A_1 -> X_2 A_3, ... A_{n-2} -> X_{n-1} X_{n}
        idx = 1
        for rule in [r for r in self.rules if (len(r.rhs) > 2)]:
            # check that we have only non-terminals
            for symb in rule.rhs:
                if symb.is_terminal():
                    raise RuntimeError("Call delete_terminals_from_rhs first!")
            # make new rules
            new_rules = []
            new_symbols = [get_new_symbol("{}_{}_{}".format(rule.lhs, k, idx))
                           for k in xrange(1, len(rule.rhs) - 1)]
            idx += 1
            # A -> X_1 A_1
            new_rules.append(Rule(rule.lhs, [rule.rhs[0], new_symbols[0]]))
            for idx in xrange(0, len(rule.rhs)-3):
                # A_k -> X_{k + 1} A_{k + 1}
                new_rules.append(Rule(new_symbols[idx],
                                      [rule.rhs[idx+1], new_symbols[idx+1]]))
            new_rules.append(Rule(new_symbols[-1], rule.rhs[-2:]))
            self.rules.remove(rule)
            self.rules.extend(new_rules)

    def convert_to_chomsky(self):
        """Chomsky Normal Form"""
        self.add_new_start_symbol()
        self.delete_terminals_from_rhs()
        self.binarize()
        self.delete_epsilon_rules()
        self.delete_unit_rules()

    def __repr__(self):
        return "S: {}, rules: {}".format(
            repr(
                self.start_symbol), repr(
                self.rules))

    def __str__(self):
        result = "Grammar, start symbol: {}:\n".format(str(self.start_symbol))
        for rule in sorted(self.rules, key=lambda x: x.lhs):
            result += str(rule) + "\n"
        return result


if __name__ == "__main__":
    symb_S = get_new_symbol("S")
    symb_A = get_new_symbol("A")
    symb_B = get_new_symbol("B")
    symb_C = get_new_symbol("C")
    symb_D = get_new_symbol("D")
    symb_a = get_new_terminal("a")
    rule_1 = Rule(symb_S, [symb_a, symb_a])
    rule_2 = Rule(symb_S, [symb_A, symb_B, symb_C])
    rule_3 = Rule(symb_S, [symb_A, symb_B, symb_C, symb_D])
    grammar_1 = Grammar([rule_1, rule_2, rule_3])
    grammar_1.set_start_symbol(symb_S)
    print(str(grammar_1))
    grammar_1.add_new_start_symbol()
    print("Added new start symbol")
    print(str(grammar_1))
    grammar_1.delete_terminals_from_rhs()
    print("Deleted terminals from rhs")
    print(str(grammar_1))
    grammar_1.binarize()
    print("Only binary and unit rules left")
    print(str(grammar_1))
    grammar_1.add_rule(symb_A, [epsilon])
    grammar_1.add_rule(symb_B, [symb_A, symb_A])
    ns = grammar_1.get_nullable_symbols()
    print("Nullable after: ", [str(s) for s in ns])
    print("Grammar with epsilon rules:")
    print(str(grammar_1))
    grammar_1.delete_epsilon_rules()
    print("Deleted epsilon rules: ")
    print(str(grammar_1))
    grammar_1.delete_unit_rules()
    print("Deleted unit rules")
    print(str(grammar_1))
